<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();
Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cleared!";
});
Route::get('/', 'HomeController@index')->name('welcome');
Route::get('/article/{id}', 'HomeController@singleArticle')->name('single.article');
Route::get('/doctor', 'HomeController@doctor')->name('doctor');
Route::get('/doctor/{id}details', 'HomeController@doctorInfo')->name('doctor.info');
Route::get('/question/answer', 'HomeController@questionPage')->name('question.answer');

Route::group(['middleware'=>['auth']], function ()
{
    Route::post('/question/comment/store','Patent\QuestionAskController@CommentStore')->name('question.comment.store');
    Route::post('/article/comment/store','ArticleCommentController@CommentStore')->name('article.comment.store');
    Route::get('/article/comment/reload{id}','ArticleCommentController@reload')->name('article.comment.reload');
    Route::get('/message/page','MessageController@messagePage')->name('message.page');
    Route::get('message/contacts','MessageController@getContacts');
    Route::get('/get/message/{id}','MessageController@getMessage');
    Route::post('/sent/message','MessageController@sentMessage');

});

Route::group(['as'=>'doctor.','prefix'=>'doctor','namespace'=>'Doctor','middleware'=>['auth','doctor']], function ()
{
    Route::get('/profile','ProfileController@index')->name('profile');
    Route::post('/article/post','ArticleController@store')->name('article.store');

});
Route::group(['as'=>'patent.','prefix'=>'patent','namespace'=>'Patent','middleware'=>['auth','patent']], function ()
{
    Route::post('/ask/question/post','QuestionAskController@store')->name('question.store');

});

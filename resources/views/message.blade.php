@extends('layouts.app')
@push('css')
    <link href="{{ asset('assets/chat/dist/css/soho.min.css') }}" rel="stylesheet">
@endpush
@section('content')
    <Chat :user="{{auth()->user()}}"></Chat>
@endsection
@push('js')
    <!-- JQuery -->
    <script src="{{asset('assets/chat/vendor/jquery-3.4.1.min.js')}}"></script>

    <!-- Popper.js -->
    <script src="{{asset('assets/chat/vendor/popper.min.js')}}"></script>

    <!-- Bootstrap -->
    <script src="{{asset('assets/chat/vendor/bootstrap/bootstrap.min.js')}}"></script>

    <!-- Nicescroll -->
    <script src="{{asset('assets/chat/vendor/jquery.nicescroll.min.js')}}"></script>

    <!-- Soho -->
    <script src="{{asset('assets/chat/dist/js/soho.min.js')}}"></script>
    <script src="{{asset('assets/chat/dist/js/examples.js')}}"></script>
@endpush

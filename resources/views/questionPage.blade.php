@extends('layouts.frontend.app')

@section('content')
    <section class="page_breadcrumbs ds background_cover section_padding_top_40 section_padding_bottom_40">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2>Ask a question</h2>
                </div>
            </div>
        </div>
    </section>
    <section class="ls section_padding_top_80 section_padding_bottom_80 columns_padding_30">
        <div class="container">
            @if (isset(auth()->user()->role) and auth()->user()->role->id == 3)
                <!-------question section ---->
                <div class="comment-respond text-center" id="respond">
                    <h3>Write your question here</h3>
                    <h6 id="status"></h6>
                    <form id="question-form" class="comment-form" method="post" action="javascript:void(0);">
                        @csrf
                        <div class="row columns_padding_10">
                            <div class="col-xs-12">
                                <div class="form-group margin_0">
                                    <label for="question_filed"></label>
                                    <textarea aria-required="true" required id="question_filed" rows="4" cols="45" name="question" class="form-control text-center" placeholder="write your question here ............."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-submit topmargin_30">
                            <input type="submit" id="question_submit" value="Submit" name="submit" class="theme_button color1 min_width_button margin_0 hidden" />
                        </div>
                    </form>
                </div>
                <!-- question section end --->
            @endif
            <div class="row">
                <div class="col-sm-10 col-sm-push-1">
                    @foreach($questions as $question)
                        <article class="vertical-item content-padding big-padding with_border single-post post">
                        <div class="item-content">
                            <div class="entry-content">
                                <p>{{$question->description}}</p>
                            </div>
                            <!-- .entry-content -->
                            <header class="entry-header">
                                <div class="comment-body">
                                    <div class="comment-meta darklinks">
                                        Posted on
                                        <span class="comment-date">
                                        <i class="fa fa-calendar highlight4 rightpadding_5" aria-hidden="true"></i>
                                        <time datetime="2017-10-03T08:50:40+00:00">
                                             {{ \Carbon\Carbon::parse($question->created_at)->format('d M, Y') }}
                                        </time>
                                    </span> </div>
                                </div>
                            </header>
                        </div>
                        <!-- .item-content -->
                    </article>
                    <div class="comments-area" id="comments">
                        <h4>Comments({{$question->comments->count()}})</h4>
                        @foreach($question->comments as $comment)
                            <ol class="comment-list">
                            <li class="comment even thread-even depth-1 parent">
                                <article class="comment">
                                    <div class="comment-author">
                                        <img class="media-object" alt="" src="{{asset('assets/frontend/images/faces/05.jpg')}}">
                                    </div>

                                    <div class="comment-body">
                                        <div class="comment-meta darklinks">
                                            <a class="author_url" rel="external nofollow" href="#">{{$comment->user->name}}</a>
                                            <span class="comment-date">
                                                <i class="fa fa-calendar highlight4 rightpadding_5" aria-hidden="true"></i>
                                                <time datetime="2017-10-03T08:50:40+00:00">{{$comment->created_at->diffForHumans()}}</time>
                                            </span>
                                        </div>

                                        <div class="comment-text">
                                            <p>{{ $comment->comment }}</p>
                                        </div> <span class="reply">
                                        <a href="#respond">
                                            <i class="fa fa-reply" aria-hidden="true"></i>
                                            <span class="sr-only">Reply</span> </a>
                                    </span>
                                    </div>
                                </article>
                                <!-- .comment-body -->

                                <!-- .comment-body
                                <ol class="children">
                                    <li class="comment byuser even depth-3">
                                        <article class="comment">
                                            <div class="comment-author">
                                                <img class="media-object" alt="" src="{{asset('assets/frontend/images/faces/06.jpg')}}">
                                            </div>
                                            <div class="comment-body">
                                                <div class="comment-meta darklinks">
                                                    <a class="author_url" rel="external nofollow" href="#">Dr. Xefer Rahman</a>
                                                    <span class="comment-date">
                                                    <i class="fa fa-calendar highlight4 rightpadding_5" aria-hidden="true"></i>
                                                    <time datetime="2017-10-03T08:50:40+00:00">
                                                        25 jan, 2019
                                                    </time>
                                                </span> </div>
                                                <div class="comment-text">
                                                    <p>Shankle ham hock chuck pork chop capicola. Ham andouille beef chicken.</p>
                                                </div> <span class="reply">
                                                <a href="#respond">
                                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                                    <span class="sr-only">Reply</span> </a>
                                            </span>
                                            </div>
                                        </article>
                                    </li>
                                </ol>
                                ----->
                            </li>
                        </ol>
                        @endforeach
                    </div>
                    @if (auth()->check())
                        <!-- #comments -->
                        <div class="comment-respond text-center" id="respond">
                            <h3>Write comment Now:</h3>
                            <form class="comment-form" id="comment-form" method="post" action="{{route('question.comment.store')}}">
                                @csrf
                                <div class="row columns_padding_10">
                                    <div class="col-xs-12">
                                        <div class="form-group margin_0">
                                            <label for="comment">Comment</label>
                                            <textarea aria-required="true" onclick="submitButton({{$question->id}});" rows="4" cols="30" name="comment" id="comment" class="form-control text-center" placeholder="write your Comment here ............."></textarea>
                                            <input type="hidden" name="question_id" value="{{$question->id}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-submit topmargin_30">
                                    <input type="submit" id="submit{{$question->id}}" name="submit" value="Submit" class="theme_button color3 min_width_button margin_0 hidden">
                                </div>
                            </form>
                        </div>
                    @endif
                    @endforeach
                </div>
            </div>

        </div>
    </section>
@endsection

@push('js')
    @if (isset(auth()->user()->role) and auth()->user()->role->id == 3)
        <script>
            $(document).ready(function(){
                $('#question_filed').on('click',function () {
                    $('#question_submit').removeClass('hidden');
                });
                $('#question-form').submit(function () {
                    $.post("<?php echo url(route('patent.question.store'))?>", $(this).serialize(),function (data) {
                        $('#status').html(data);
                        $('#question_filed').val('');
                        $('#question_submit').addClass('hidden');
                    });
                    return false;
                });
            });
        </script>
    @endif
    @if (auth()->check())
        <script>
            function submitButton(id){
                $('#submit'+id).removeClass('hidden');
            }
        </script>
    @endif
@endpush

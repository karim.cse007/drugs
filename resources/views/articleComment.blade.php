@foreach($comments as $comment)
    <ol class="comment-list">
        <li class="comment even thread-even depth-1 parent">
            <article class="comment">
                <div class="comment-author"><img class="media-object" alt=""
                                                 src="{{asset('assets/frontend/images/faces/04.jpg')}}"></div>
                <div class="comment-body">
                    <div class="comment-meta darklinks">
                        <a class="author_url" rel="external nofollow" href="#">{{$comment->user->name}}</a>
                        <span class="comment-date">
                            <i class="fa fa-calendar highlight4 rightpadding_5"
                               aria-hidden="true"></i>
                            <time datetime="2017-10-03T08:50:40+00:00">
                                {{$comment->created_at->diffForHumans()}}
                            </time>
                        </span>
                    </div>
                    <div class="comment-text">
                        <p>{{$comment->comment}}</p>
                    </div>
                    <span class="reply">
                        <a href="#respond">
                            <i class="fa fa-reply" aria-hidden="true"></i>
                        <span class="sr-only">Reply</span> </a>
                    </span>
                </div>
            </article>
        <!-- .comment-body
                                <ol class="children">
                                    <li class="comment byuser odd alt depth-2 parent">
                                        <article class="comment">
                                            <div class="comment-author">
                                                <img class="media-object" alt="" src="{{asset('assets/frontend/images/faces/05.jpg')}}">
                                            </div>
                                            <div class="comment-body">
                                                <div class="comment-meta darklinks"> <a class="author_url" rel="external nofollow" href="#">Devin McGee</a>
                                                    <span class="comment-date">
													<i class="fa fa-calendar highlight4 rightpadding_5" aria-hidden="true"></i>
													<time datetime="2017-10-03T08:50:40+00:00">
														25 jan, 2018
													</time>
												</span> </div>
                                                <div class="comment-text">
                                                    <p>Shankle ham hock chuck pork chop capicola. Ham andouille beef chicken.</p>
                                                </div> <span class="reply">
												<a href="#respond">
													<i class="fa fa-reply" aria-hidden="true"></i>
													<span class="sr-only">Reply</span> </a>
														</span>
                                            </div>
                                        </article>
                                    </li>
                                </ol>
                                ----->
        </li>
        <!-- #comment-## -->
    </ol>
@endforeach
<!-- .comment-list -->

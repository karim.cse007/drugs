@extends('layouts.frontend.app')

@section('content')
    <section id="blog" class="ls section_padding_top_90 section_padding_bottom_110">
        <div class="container">
            @if (isset(auth()->user()->role) and auth()->user()->role->id == 2)
                <section id="article-post-section" class="section_padding_left_120">
                    <form class="form-group col-md-12" method="post" action="{{route('doctor.article.store')}}" enctype="multipart/form-data">
                        @csrf
                        <input type="text" name="title" id="title" class="hidden form-control" placeholder="write your title">
                        <textarea id="article-area-post" name="article" class="form-control" placeholder="What's in your mind" cols="4" rows="3"></textarea>
                        <input type="file" class="hidden form-control" id="image" name="image">
                        <input type="submit" id="submit-article" class="hidden form-control" value="post"/>
                    </form>
                </section>
            @endif
            <div class="row flex-wrap v-center">
                @foreach($articles as $key=>$article)
                    <article class="post format-small-image topmargin_30 to_animate" data-animation="{{$key/2==0?'fadeInRight':'fadeInLeft'}}">
                        <div
                            class="side-item side-md content-padding big-padding with_border bottom_color_border {{$key/2==0?'left':'right'}}">
                            <div class="row">
                                @if ($article->image !=null)
                                    <div class="col-md-5 col-lg-4">
                                        <div class="item-media entry-thumbnail">
                                            <img src="{{asset('storage/article/'.$article->image)}}" alt="">
                                        </div>
                                    </div>
                                @endif
                                <div class="{{$article->image==null?'col-md-12':'col-md-7 col-lg-8'}}">
                                    <div class="item-content">
                                        <header class="entry-header">
                                            <h3 class="entry-title small">
                                                <a href="{{route('single.article',$article->id)}}">
                                                    {{$article->title}}
                                                </a>
                                            </h3>
                                            <div class="entry-meta inline-content greylinks">
                                                <span>
                                                    <i class="fa fa-calendar highlight rightpadding_5" aria-hidden="true"></i>
                                                    <time datetime="2017-10-03">
                                                        {{ \Carbon\Carbon::parse($article->created_at)->format('d M, Y') }}
                                                    </time>
                                                    </span>by<a href="{{route('doctor.info',$article->doctor->id)}}">
                                                        {{$article->doctor->user->name}}
                                                        </a>
                                                    <span>
                                                </span>
                                            </div>
                                        </header>
                                        <div class="entry-content md-content-3lines-ellipsis">
                                            <p>{{ $article->description }}<a href="{{route('single.article',$article->id)}}"> >>Read More<< </a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                @endforeach
            </div>
        </div>
    </section>
@endsection

@push('js')
    @if (isset(auth()->user()->role) and auth()->user()->role->id == 2)
        <script>
            $(document).ready(function(){
                $('#article-area-post').on('click',function () {
                    $('#title').removeClass('hidden');
                    $('#image').removeClass('hidden');
                    $('#submit-article').removeClass('hidden');
                });
            });
        </script>
    @endif
@endpush

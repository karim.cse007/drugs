@extends('layouts.frontend.app')

@section('content')
    <section id="blog" class="ls section_padding_top_90 section_padding_bottom_110">
        <div class="container">
            <section id="post" class="section_padding_left_120">
                <div class="text">
                    <textarea placeholder="What's in your mind" style="width: 90%;" rows="4"></textarea>
                    <input type="submit" value="post" />
                </div>
            </section>
            <div class="row flex-wrap v-center">
                <article class="post format-small-image topmargin_30 to_animate" data-animation="fadeInRight">
                    <div class="side-item side-md content-padding big-padding with_border bottom_color_border left">
                        <div class="row">
                            <div class="col-md-5 col-lg-4">
                                <div class="item-media entry-thumbnail">
                                    <img src="{{asset('assets/frontend/images/events/09.jpg')}}" alt="">
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-8">
                                <div class="item-content">
                                    <header class="entry-header">
                                        <h3 class="entry-title small">
                                            <a href="{{route('doctor.info','1')}}" rel="bookmark">Dr. Abdul Karim</a>
                                        </h3>
                                        <div class="entry-meta inline-content greylinks"> <span>
                                            <i class="fa fa-calendar highlight rightpadding_5" aria-hidden="true"></i>
                                            <a href="#">
                                                <time datetime="2017-10-03T08:50:40+00:00">
                                                    17 jan, 2018</time>
                                            </a>
                                        </span> <span>
                                        </span> </div>
                                    </header>
                                    <div class="entry-content md-content-3lines-ellipsis">
                                        <p>Nostrud ham hock fatback aute tri-tip sausage doner mollit quis laboris. Jerky meatball cupim filet mignon. Prosciutto rump eu fatback andouille eiusmod strip steak. Fugiat shoulder salami chuck in mollit. Minim nulla beef ribs adipisicing.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

                <article class="post format-small-image topmargin_30 to_animate" data-animation="fadeInLeft">
                    <div class="side-item side-md content-padding big-padding with_border bottom_color_border right">
                        <div class="row">
                            <div class="col-md-5 col-lg-4">
                                <div class="item-media entry-thumbnail"> <img src="{{asset('assets/frontend/images/events/08.jpg')}}" alt=""> </div>
                            </div>
                            <div class="col-md-7 col-lg-8">
                                <div class="item-content">
                                    <header class="entry-header">
                                        <h3 class="entry-title small">
                                            <a href="#" rel="bookmark">Dr. Abdul Hamid</a>
                                        </h3>
                                        <div class="entry-meta inline-content greylinks"> <span>
                                            <i class="fa fa-calendar highlight rightpadding_5" aria-hidden="true"></i>
                                            <a href="#">
                                                <time datetime="2017-10-03T08:50:40+00:00">
                                                    17 jan, 2018</time>
                                            </a>
                                        </span>
                                        </div>
                                    </header>
                                    <div class="entry-content md-content-3lines-ellipsis">
                                        <p>Qui laboris ut, duis excepteur fatback boudin nostrud esse ea pork chop cupidatat cupim. Picanh landjaeger buffalo consequat jerky brisket, cupim ribeye magna beef occaecat. Venison salami pancetta t-bone ut, exercitation aliqua.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>
@endsection

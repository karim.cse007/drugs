<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function doctor(){
        return $this->belongsTo(Doctor::class);
    }
    public function comments(){
        return $this->hasMany(Comment::class);
    }
}

<?php

namespace App\Http\Controllers;

use App\Events\MessageEvent;
use App\Message;
use App\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function messagePage(){
        return view('message');
    }
    public function getContacts()
    {
        $users = User::where('id','!=',auth()->id())->get();
        return response()->json($users,200);
        $unreadIds = Message::select(DB::raw('`sender_id` as sender_id, count(`sender_id`) as messages_count'))
            ->where('receiver_id', auth()->id())
            ->where('is_seen', false)
            ->groupBy('sender_id')
            ->get();
        $users = $users->map(function($user) use ($unreadIds) {
            $contactUnread = $unreadIds->where('sender_id', $user->id)->first();

            $user->unread = $contactUnread ? $contactUnread->messages_count : 0;

            return $user;
        });

        return response()->json($users,200);
    }
    public function getMessage($id)
    {
        // mark all messages with the selected contact as read
        Message::where('sender_id', $id)->where('receiver_id', auth()->id())->update(['is_seen' => true]);

        $message = Message::where(function ($q) use ($id) {
            $q->where('sender_id',auth()->id());
            $q->where('receiver_id',$id);
        })->orWhere(function ($q) use ($id) {
            $q->where('sender_id',$id);
            $q->where('receiver_id',auth()->id());
        })->get();
        return response()->json($message,200);
    }
    public function sentMessage(Request $request)
    {
        $message = new Message();
        $message->sender_id = auth()->id();
        $message->receiver_id = $request->contact_id;
        $message->message = $request->message;
        $message->save();
        broadcast(new MessageEvent($message->load('fromContact')));
        return response()->json($message,200);
    }
}

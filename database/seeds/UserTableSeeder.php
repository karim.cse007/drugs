<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id'=>1,
            'name'=>'Admin',
            'email'=>'admin@email.com',
            'password'=>bcrypt('rootadmin'),
        ]);
        DB::table('users')->insert([
            'role_id'=>2,
            'name'=>'Doctor',
            'email'=>'doctor@email.com',
            'password'=>bcrypt('rootdoctor'),
        ]);
        DB::table('users')->insert([
            'role_id'=>3,
            'name'=>'Patient',
            'email'=>'patient@email.com',
            'password'=>bcrypt('rootpatient'),
        ]);
    }
}
